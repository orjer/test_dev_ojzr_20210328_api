﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TEST_DEV_OJZR.Models {

    [MetadataType(typeof(Tb_PersonasFisicas.Metadata))]
    public partial class Tb_PersonasFisicas {
        sealed class Metadata {

            [Required]
            public string Nombre;

            [Required]
            public string ApellidoPaterno;

            [Required]
            public string ApellidoMaterno;
            
            [StringLength(13, MinimumLength = 13) ]
            public string RFC;
        }
        
    }
}