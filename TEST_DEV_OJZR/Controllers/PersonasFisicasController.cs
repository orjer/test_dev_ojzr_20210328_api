﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using TEST_DEV_OJZR.Models;

namespace TEST_DEV_OJZR.Controllers {
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PersonasFisicasController : ApiController {
        private TEST_DEVEntities db = new TEST_DEVEntities();

        // GET: api/PersonasFisicas
        public IQueryable<Tb_PersonasFisicas> GetTb_PersonasFisicas() {
            return db.Tb_PersonasFisicas.Where( x => x.Activo == true );
        }

        // GET: api/PersonasFisicas/5
        [ResponseType(typeof(Tb_PersonasFisicas))]
        public IHttpActionResult GetTb_PersonasFisicas(int id) {
            Tb_PersonasFisicas tb_PersonasFisicas = db.Tb_PersonasFisicas.Find(id);
            if (tb_PersonasFisicas == null) {
                return NotFound();
            }

            return Ok(tb_PersonasFisicas);
        }

        // PUT: api/PersonasFisicas/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTb_PersonasFisicas(int id, Tb_PersonasFisicas persona) {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }
            sp_ActualizarPersonaFisica_Result result = db.sp_ActualizarPersonaFisica(
                id,
                persona.Nombre,
                persona.ApellidoPaterno,
                persona.ApellidoMaterno,
                persona.RFC,
                persona.FechaNacimiento,
                persona.UsuarioAgrega).First();

            if (result.ERROR < 0) {
                return BadRequest(result.MENSAJEERROR);
            }

            Tb_PersonasFisicas personaFisica = db.Tb_PersonasFisicas.Find(result.ERROR);
            return Ok(personaFisica);
        }

        // POST: api/PersonasFisicas
        [ResponseType(typeof(Tb_PersonasFisicas))]
        public IHttpActionResult PostTb_PersonasFisicas(Tb_PersonasFisicas persona) {
            if (!ModelState.IsValid){
                return BadRequest( ModelState );
            }
            sp_AgregarPersonaFisica_Result result = db.sp_AgregarPersonaFisica( 
                persona.Nombre, 
                persona.ApellidoPaterno, 
                persona.ApellidoMaterno,
                persona.RFC,
                persona.FechaNacimiento,
                persona.UsuarioAgrega).First();

            if( result.ERROR < 0) {
                return BadRequest( result.MENSAJEERROR );
            }

            Tb_PersonasFisicas personaFisica = db.Tb_PersonasFisicas.Find( result.ERROR );
            return Ok( personaFisica );
        }

        // DELETE: api/PersonasFisicas/5
        [ResponseType(typeof(Tb_PersonasFisicas))]
        public IHttpActionResult DeleteTb_PersonasFisicas(int id) {

            sp_EliminarPersonaFisica_Result result = db.sp_EliminarPersonaFisica(id).First();

            if (result.ERROR < 0) {
                return BadRequest(result.MENSAJEERROR);
            }

            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Tb_PersonasFisicasExists(int id)
        {
            return db.Tb_PersonasFisicas.Count(e => e.IdPersonaFisica == id) > 0;
        }
    }
}